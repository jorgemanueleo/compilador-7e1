﻿Public Class Form1
    Public objeto(100000) As tabla_simbolos ' arreglo de objetos de la clase tabla de simbolos
    Public errorsin As New manejo_de_errores 'objeto para mandar llamar los errores
    Dim texto() As String, repetido As String
    Public contador_nodos As Integer = 0 'el contador de nodos creados de nuestro arreglo para la tabla de simbolos  
    Dim es_simb As Integer = 0, es_reservada As Integer = 0
    Dim cont_cola As Integer = 0, pared2(100000) As TextBox, bandera_guardar(10000000) As Integer
    Dim nom_archivo(100000) As String ' cuando ya se guarda el archivo se almacena aqui para cuando se vuelve a guardar ya no pregunte el nombre del archivo
    Dim palabra As Integer
    Dim contenido() As String, contenido2() As String, contador_espacios As Integer = 0
    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close() ' cierra el form 
    End Sub

    Private Sub ShellToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShellToolStripMenuItem.Click
        System.Diagnostics.Process.Start("programa1_11300293\Debug\programa1_11300293.exe") 'ejecuta el programa con el interprete 
    End Sub

    Private Sub CopiarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CopiarToolStripMenuItem.Click
        If cont_cola > 0 Then ' si no hay texbox para copiar no hace nada
            Clipboard.SetDataObject(pared2(cont_cola).SelectedText) ' agarra lo seleccionado y lo pone en el porta papeles
        Else
            CopiarToolStripMenuItem.Enabled = False
        End If

    End Sub

    Private Sub CortarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CortarToolStripMenuItem.Click
        If cont_cola > 0 Then ' si no hay textbox no corta nada y si hay se llama la funcion cortar
            pared2(cont_cola).Cut()
        Else
            CortarToolStripMenuItem.Enabled = False
        End If

    End Sub

    Private Sub PegarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PegarToolStripMenuItem.Click
        If cont_cola > 0 Then ' si no hay textbox no corta nada y si hay se llama la funcion pegar
            pared2(cont_cola).Paste()
        Else
            PegarToolStripMenuItem.Enabled = False
        End If


    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' los valores iniciales, como al principio no hay textbox las opc estan desactivadas
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location ' se acopla el form al tamaño del escritorio
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
        CopiarToolStripMenuItem.Enabled = False
        PegarToolStripMenuItem.Enabled = False
        CortarToolStripMenuItem.Enabled = False
        CompilarToolStripMenuItem.Enabled = False
        GuardarComoToolStripMenuItem.Enabled = False
        GuardarToolStripMenuItem.Enabled = False
        CerrarToolStripMenuItem.Enabled = False

    End Sub

    Private Sub AcercaDeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AcercaDeToolStripMenuItem.Click
        MessageBox.Show("IDE version 1.0" & vbCrLf & "By Jorge Manuel Escamilla Ornelas 11300293") 'mensaje con mis datos
    End Sub

    Private Sub AbrirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AbrirToolStripMenuItem.Click

        CopiarToolStripMenuItem.Enabled = True
        PegarToolStripMenuItem.Enabled = True
        CortarToolStripMenuItem.Enabled = True
        CompilarToolStripMenuItem.Enabled = True
        GuardarComoToolStripMenuItem.Enabled = True
        GuardarToolStripMenuItem.Enabled = True
        CerrarToolStripMenuItem.Enabled = True
        Dim objeto As New OpenFileDialog ' se abre el archivo de texto pero previamente se crea un textbox para poner el texto ahi
        cont_cola += 1
        pared2(cont_cola) = New TextBox
        With pared2(cont_cola)
            .Size = Screen.PrimaryScreen.WorkingArea.Size
            .Multiline = True
            .Top = 25
            .Left = 0
            .Visible = True
            .Font = New Font("Arial", 16, FontStyle.Regular, GraphicsUnit.Pixel)
            .ForeColor = Color.Blue
        End With
        Controls.Add(pared2(cont_cola)) ' se agrega el textbox creado al form
        pared2(cont_cola).BringToFront()
        If objeto.ShowDialog() = System.Windows.Forms.DialogResult.OK Then ' si se dio clic en ok al abrir el archivo...
            Dim sr As New System.IO.StreamReader(objeto.FileName) ' se crea un objeto fichero el cual accedera al contenido del archivo para post ponerlo en el textbox
            nom_archivo(cont_cola) = objeto.FileName
            pared2(cont_cola).Text = (sr.ReadToEnd)
            sr.Close()
            bandera_guardar(cont_cola) = 1
        ElseIf objeto.ShowDialog() = System.Windows.Forms.DialogResult.Cancel Then
        End If

    End Sub

    Private Sub GuardarComoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarComoToolStripMenuItem.Click

        If cont_cola > 0 Then
            Call nguar() ' si hay algun archivo abierto,  se manda a llamar la funcion de guardar 
        Else
            GuardarComoToolStripMenuItem.Enabled = False
        End If

    End Sub
    Private Sub nguar()
        Dim objeto As New SaveFileDialog ' creo un nuevo cuadro de dialogo para guardar 
        objeto.Filter = "txt files (*.flow)|*.flow|All files (*.*)|*.*"  ' se especifica lo que va a contener el cuadro para guardar el archivo
        objeto.DefaultExt = "flow"
        objeto.Title = "Guardar Como..."
        objeto.CreatePrompt = True
        objeto.OverwritePrompt = True
        DialogResult = objeto.ShowDialog
        If DialogResult = Windows.Forms.DialogResult.OK Then ' si se presiona ok se abrira el cuadro de dialogo para guardar el archivo
            Dim objescribir As New System.IO.StreamWriter(objeto.FileName) ' se crea un objeto stream para poder asignarle el texto
            ' escrito en el textbox actual, se manejan arreglos para los textbos porque busco tener varios abiertos al mismo tiempo
            nom_archivo(cont_cola) = objeto.FileName
            For Each caracter As Char In (pared2(cont_cola).Text)
                objescribir.Write(caracter)
            Next
            objescribir.Close()
            bandera_guardar(cont_cola) = 1
        ElseIf DialogResult = Windows.Forms.DialogResult.Cancel Then
            bandera_guardar(cont_cola) = 0
        End If
    End Sub
    Private Sub nguar1() ' este procedimiento guarda el archivo mediante el nombre que ya se habia almacenado cuando se guardo previamente
        Dim esc2 As New System.IO.StreamWriter(nom_archivo(cont_cola))
        For Each caracter As Char In (pared2(cont_cola).Text)
            esc2.Write(caracter)
        Next
        esc2.Close()
        bandera_guardar(cont_cola) = 1
    End Sub
    Private Sub nguar3()

        Call nguar4() ' mandamos llamar esta para guardar el .flow, posteriormente se remplaza el flow por cpp y se guarda 
        nom_archivo(cont_cola) = nom_archivo(cont_cola).Replace("flow", "cpp")

        Dim esc3 As New System.IO.StreamWriter(nom_archivo(cont_cola))
        For x As Integer = 0 To contador_espacios
            For Each caracter As Char In texto(x)
                esc3.Write(caracter)
            Next
        Next x
        esc3.Close()
        bandera_guardar(cont_cola) = 1

    End Sub
    Private Sub nguar2()

        Dim objeto As New SaveFileDialog ' creo un nuevo cuadro de dialogo para guardar 
        objeto.Filter = "txt files (*.flow)|*.flow|All files (*.*)|*.*"  ' se especifica lo que va a contener el cuadro para guardar el archivo
        objeto.DefaultExt = "flow"
        objeto.Title = "Guardar Como..."
        objeto.CreatePrompt = True
        objeto.OverwritePrompt = True
        DialogResult = objeto.ShowDialog

        If DialogResult = Windows.Forms.DialogResult.OK Then ' si se presiona ok se abrira el cuadro de dialogo para guardar el archivo
            Dim objescribir As New System.IO.StreamWriter(objeto.FileName) ' se crea un objeto stream para poder asignarle el texto
            ' escrito en el textbox actual, se manejan arreglos para los textbos porque busco tener varios abiertos al mismo tiempo
            nom_archivo(cont_cola) = objeto.FileName
            objescribir.WriteLine(pared2(cont_cola).Text)
            objescribir.Close()

            objeto.FileName = objeto.FileName.Replace("flow", "cpp")

            Dim esc2 As New System.IO.StreamWriter(objeto.FileName)

            For x As Integer = 0 To contador_espacios
                For Each caracter As Char In texto(x)
                    esc2.Write(caracter)
                Next
            Next x

            esc2.Close()
            bandera_guardar(cont_cola) = 1
        ElseIf DialogResult = Windows.Forms.DialogResult.Cancel Then
            bandera_guardar(cont_cola) = 0
        End If



    End Sub
    Private Sub nguar4()

        Dim objeto As New SaveFileDialog ' creo un nuevo cuadro de dialogo para guardar 
        objeto.Filter = "txt files (*.flow)|*.flow|All files (*.*)|*.*"  ' se especifica lo que va a contener el cuadro para guardar el archivo
        objeto.DefaultExt = "flow"
        objeto.Title = "Guardar..."
        objeto.CreatePrompt = True
        objeto.FileName = nom_archivo(cont_cola)
        objeto.OverwritePrompt = True
        DialogResult = objeto.ShowDialog

        If DialogResult = Windows.Forms.DialogResult.OK Then ' si se presiona ok se abrira el cuadro de dialogo para guardar el archivo
            Dim objescribir As New System.IO.StreamWriter(objeto.FileName) ' se crea un objeto stream para poder asignarle el texto
            ' escrito en el textbox actual, se manejan arreglos para los textbos porque busco tener varios abiertos al mismo tiempo
            nom_archivo(cont_cola) = objeto.FileName
            objescribir.WriteLine(pared2(cont_cola).Text)
            objescribir.Close()

            objeto.FileName = objeto.FileName.Replace("flow", "cpp")

            Dim esc2 As New System.IO.StreamWriter(objeto.FileName)

            For x As Integer = 0 To contador_espacios
                For Each caracter As Char In texto(x)
                    esc2.Write(caracter)
                Next
            Next x

            esc2.Close()
            bandera_guardar(cont_cola) = 1
        ElseIf DialogResult = Windows.Forms.DialogResult.Cancel Then
            bandera_guardar(cont_cola) = 0
        End If



    End Sub
    Private Sub NuevoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NuevoToolStripMenuItem.Click

        CopiarToolStripMenuItem.Enabled = True
        PegarToolStripMenuItem.Enabled = True
        CortarToolStripMenuItem.Enabled = True
        CompilarToolStripMenuItem.Enabled = True
        GuardarComoToolStripMenuItem.Enabled = True
        GuardarToolStripMenuItem.Enabled = True
        CerrarToolStripMenuItem.Enabled = True
        cont_cola += 1
        pared2(cont_cola) = New TextBox
        With pared2(cont_cola)
            .Size = Screen.PrimaryScreen.WorkingArea.Size
            .Multiline = True
            .Top = 25
            .Left = 0
            .Visible = True
            .Font = New Font("Arial", 16, FontStyle.Regular, GraphicsUnit.Pixel)
            .ForeColor = Color.Blue
        End With
        Controls.Add(pared2(cont_cola))
        pared2(cont_cola).BringToFront()
        ' simplemente se crea un textbox nuevo y en el siguiente arreglo lo uso como bandera para indicar que no se a guardado el archivo actual
        bandera_guardar(cont_cola) = 0

    End Sub

    Private Sub CerrarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CerrarToolStripMenuItem.Click

        If cont_cola > 0 Then
            If bandera_guardar(cont_cola) = 0 Then
                If MessageBox.Show("Desea guardar los cambios?", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    Call nguar()
                    Controls.Remove(pared2(cont_cola))
                    cont_cola -= 1
                    ' en cerrar lo que hago es que si no se a guardado el archivo previamente se manda a llamar la funcion para que lo guarde
                ElseIf DialogResult.No Then
                    Controls.Remove(pared2(cont_cola))
                    cont_cola -= 1

                End If
            ElseIf bandera_guardar(cont_cola) = 1 Then ' si ya se a guardado el archivo se cierra en caso de que el usuario lo quiera 
                Controls.Remove(pared2(cont_cola))
                cont_cola -= 1
            End If

        Else
            CerrarToolStripMenuItem.Enabled = False
        End If

    End Sub


    Private Sub GuardarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarToolStripMenuItem.Click
        If cont_cola > 0 Then ' si hay algun textbox: 
            If bandera_guardar(cont_cola) = 1 Then ' si ya se guardo previamente solo se utiliza el path para volver a guardar los cambios
                nguar1()
            ElseIf bandera_guardar(cont_cola) = 0 Then
                nguar() ' si no se a guardado el archivo simplemente llamamos la funcion guardar para que lo guarde por primera vez
            End If

        End If

    End Sub

    Private Sub AyudaToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AyudaToolStripMenuItem1.Click
        System.Diagnostics.Process.Start("ayuda.chm") 'se manda a llamar el archivo que contiene la ayuda de este ide
    End Sub

    Private Sub CompilarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CompilarToolStripMenuItem.Click
        Dim flag1 As Integer = 0
        Dim contador_es As Integer
        contador_nodos = 0
        contador_es = 0
        objeto(0) = New tabla_simbolos
        palabra = 0
        contador_espacios = 0
        es_reservada = 0
        es_simb = 0


        
        MessageBox.Show("Primero guarde los cambios!") ' si no se a guardado el archivo simplemente llamamos la funcion guardar para que lo guarde por primera vez
           

        pared2(cont_cola).Text = pared2(cont_cola).Text.Replace(vbCrLf, " ")
        'se van a remplazar los saltos de linea por un espacio para poder almacenar las palabras en el string
        contenido = pared2(cont_cola).Text.Split(New Char() {" ", ";"})
        'no se almacenaran n los espacios ni los punto y coma
        For x As Integer = 0 To pared2(cont_cola).TextLength - 1
            If pared2(cont_cola).Text(x) = " " Or pared2(cont_cola).Text(x) = ";" Then
                contador_espacios += 1
                ' mientras que se lee un espacio o un ; significa que es una nueva palabra
            End If
        Next x

        Call checar_palabra(contenido, contador_espacios)
        texto = pared2(cont_cola).Text.Split(New Char() {" ", ";"})
        'pared2(cont_cola).Text += vbCrLf & vbCrLf & "Codigo en .cpp: " & vbCrLf & vbCrLf
        Call traducir_palabra(texto, contador_espacios)

        If contador_nodos > 1 Then
            For y As Integer = 0 To contador_nodos - 1   'SE MUESTRA LA TABLA DE SIMBOLOS CON SUS CAMPOS
                'MessageBox.Show(objeto(y).tipo)             'TIPO DE DATO, NOMBRE DE VARIABLE Y VALOR
                'MessageBox.Show(objeto(y).nombre)
                'MessageBox.Show(objeto(y).valor)
            Next y
        End If

        ' en este for se comprueba si el nombre de la variable ya se declaro previamente y manda llamar un mensaje de error de nuestra clase para el manejo de errores
        For x As Integer = 0 To palabra - 1
            For y As Integer = 0 To palabra - 1
                If objeto(x).nombre = objeto(y).nombre Then
                    flag1 += 1
                End If
            Next y
        Next x
        If flag1 > palabra Then
            errorsin.error1()
            'MessageBox.Show(Convert.ToString(flag1))
            flag1 = 0
        End If

        If cont_cola > 0 Then ' si hay algun textbox: 
            If bandera_guardar(cont_cola) = 1 Then ' si ya se guardo previamente solo se utiliza el path para volver a guardar los cambios
                nguar3()
            ElseIf bandera_guardar(cont_cola) = 0 Then
                nguar2() ' si no se a guardado el archivo simplemente llamamos la funcion guardar para que lo guarde por primera vez
            End If

        End If

    End Sub

    Private Sub checar_palabra(ByRef contenido() As String, ByVal contador_espacios As Integer)

        For x As Integer = 0 To contador_espacios
            'MessageBox.Show(contenido(x))
            If contenido(x) = "si" Then
                Call llenar_tabla_res("if", contenido, x)
                es_reservada = 1
            End If
            If contenido(x) = "sino" Then
                Call llenar_tabla_res("if-else", contenido, x)
                es_reservada = 1
            End If
            If contenido(x) = "para" Then
                Call llenar_tabla_res("for", contenido, x)
                es_reservada = 1
            End If
            If contenido(x) = "haz" Then
                Call llenar_tabla_res("do", contenido, x)
                es_reservada = 1
            End If
            If contenido(x) = "mientras" Then
                Call llenar_tabla_res("while", contenido, x)
                es_reservada = 1
            End If
            If contenido(x) = "fn" Then
                Call llenar_tabla_res("function", contenido, x)
                es_reservada = 1
            End If
            If contenido(x) = "+" Then
                Call llenar_tabla_res("+", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = "-" Then
                Call llenar_tabla_res("-", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = "*" Then
                Call llenar_tabla_res("*", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = "/" Then
                Call llenar_tabla_res("/", contenido, x)
                es_simb = 1
            End If
            'If contenido(x) = "=" Then
            '    Call llenar_tabla_res("=", contenido, x)
            '    es_simb = 1
            'End If
            If contenido(x) = "==" Then
                Call llenar_tabla_res("==", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = "!=" Then
                Call llenar_tabla_res("!=", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = ">" Then
                Call llenar_tabla_res(">", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = "<" Then
                Call llenar_tabla_res("<", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = ">=" Then
                Call llenar_tabla_res(">=", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = "<=" Then
                Call llenar_tabla_res("<=", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = "@" Then
                Call llenar_tabla_res("//", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = "(" Then
                Call llenar_tabla_res("[", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = ")" Then
                Call llenar_tabla_res("]", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = "{" Then
                Call llenar_tabla_res("{", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = "}" Then
                Call llenar_tabla_res("}", contenido, x)
                es_simb = 1
            End If
            If contenido(x) = ";" Then
                Call llenar_tabla_res(";", contenido, x)
                es_simb = 1
            End If

            If es_reservada = 0 And es_simb = 0 Then
                If contenido(x) = "ent" Then
                    Call llenar_tabla("int", contenido, x)
                End If
                If contenido(x) = "car" Then
                    Call llenar_tabla("char", contenido, x)
                End If
                If contenido(x) = "flot" Then
                    Call llenar_tabla("float", contenido, x)
                End If
            End If

            es_reservada = 0
            es_simb = 0

        Next x



    End Sub
    Private Sub llenar_tabla(ByVal tipo As String, ByVal contenido() As String, ByVal x As Integer)
        palabra += 1
        objeto(contador_nodos) = New tabla_simbolos
        objeto(contador_nodos).tipo = tipo
        objeto(contador_nodos).sig = contador_nodos + 1
        objeto(contador_nodos).nombre = contenido(x + 1)
        objeto(contador_nodos).estado = "variable"
        If contenido(x + 2) <> "=" Then
            objeto(contador_nodos).valor = 0
        Else
            objeto(contador_nodos).valor = contenido(x + 3)
        End If
        'MessageBox.Show("nodos" & contador_nodos)
        contador_nodos += 1
        ' palabra += 1
        'MessageBox.Show("nodos" & contador_nodos)
    End Sub
    Private Sub llenar_tabla_res(ByVal tipo As String, ByVal contenido() As String, ByVal x As Integer)
        palabra += 1
        objeto(contador_nodos) = New tabla_simbolos
        objeto(contador_nodos).tipo = Nothing
        objeto(contador_nodos).nombre = tipo
        objeto(contador_nodos).valor = Nothing
        objeto(contador_nodos).estado = "reservada"
        objeto(contador_nodos).sig = contador_nodos + 1
        contador_nodos += 1
    End Sub
    Private Sub traducir_palabra(ByRef texto() As String, ByVal contador_espacios As Integer)

        For x As Integer = 0 To contador_espacios
            If texto(x) = "si" Then
                texto(x) = "if"
            End If
            If texto(x) = "sino" Then
                texto(x) = "ifelse"
            End If
            If texto(x) = "para" Then
                texto(x) = "for"
            End If
            If texto(x) = "haz" Then
                texto(x) = "do"
            End If
            If texto(x) = "mientras" Then
                texto(x) = "while"
            End If
            If texto(x) = "fn" Then
                texto(x) = "function"
            End If
            If texto(x) = "ent" Then
                If texto(x + 1) = "ent" Or texto(x + 1) = "flot" Or texto(x + 1) = "car" Then
                    errorsin.error2()
                End If
                texto(x) = "int"
            End If
            If texto(x) = "car" Then
                texto(x) = "char"
            End If
            If texto(x) = "flot" Then
                texto(x) = "float"
            End If

            texto(x) += " "
            contador_espacios += 1
        Next x

    End Sub

    Private Sub SaveFileDialog1_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles SaveFileDialog1.FileOk

    End Sub

End Class

